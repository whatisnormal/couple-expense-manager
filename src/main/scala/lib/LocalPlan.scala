package lib

import api.Plan
import api.models.{EnrichedExpense, Expense}
import api.models.config.Metadata

class LocalPlan(private val metadata : Metadata, private val expenses : Seq[Expense]) extends Plan{

  override def doWork(): Map[String, Double] = {



    def enrich(): Seq[EnrichedExpense]  ={

      expenses.map(expense => {

        val remainingAmount = expense.divisonType match {
          case "half" => expense.fullAmount * 0.5
          case "split" => {

            if (expense.owner.equalsIgnoreCase( metadata.details.main)) {
              expense.fullAmount *  metadata.details.splitPercentage
            } else {
              expense.fullAmount *  (1 - metadata.details.splitPercentage)
            }

          }
          case _ => expense.fullAmount
        }
        new EnrichedExpense(expense, remainingAmount)
      })
    }

    enrich().groupBy(expense => expense.owner).mapValues(expense => expense.map(_.remainingToOther).sum)
  }
}
