package lib

import java.util

import api.models.{EnrichedExpense, Expense}
import api.models.config.Metadata

object Calculator{

   def apply( metadata : Metadata,  expenses : Seq[Expense]): Map[String, Double] = {
     remainingAmountList(metadata, expenses).groupBy(expense => getOtherMember(metadata, expense.owner)).mapValues(expense => expense.map(_.remainingToOther).sum)
  }

  def remainingAmountList( metadata : Metadata,  expenses : Seq[Expense]): Seq[EnrichedExpense]  ={

    expenses.map(expense => {

      val remainingAmount = expense.divisonType match {
        case "half" => expense.fullAmount * 0.5
        case "split" =>
          if (expense.owner.equalsIgnoreCase( metadata.mainMember)) {
            expense.fullAmount *  metadata.splitPercentage
          } else expense.fullAmount *  (1 - metadata.splitPercentage)
        case _ => expense.fullAmount
      }
      new EnrichedExpense(expense, remainingAmount)
    })
  }

  private def getOtherMember( metadata : Metadata, expenseOwner : String) : String = {
    val list = new util.LinkedList(metadata.members)
    list.remove(expenseOwner)
    list.removeLast()
  }
}
