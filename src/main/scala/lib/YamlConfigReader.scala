package lib

import api.models.config.Metadata
import org.yaml.snakeyaml.Yaml
import org.yaml.snakeyaml.constructor.Constructor

import scala.io.Source

/**
  * FIXME
  *
  * @author José Fernandes (jose.fernandes@feedzai.com)
  * @since @@@feedzai.next.release@@@
  */
object YamlConfigReader {

  def apply(path: String) : Metadata = {
    val yaml = new Yaml(new Constructor(classOf[Metadata]))
    yaml.load(Source.fromFile(path).bufferedReader()).asInstanceOf[Metadata]
  }
}
