package lib

import java.text.SimpleDateFormat

import api.adapters.InputReader
import api.models.Expense
import java.text.SimpleDateFormat

import io.Source._
import scala.collection.mutable.ListBuffer
import java.nio.file.{Files, Paths}

import scala.io.{BufferedSource, Source}


class CsvReader(var bufferedSource : BufferedSource) extends InputReader{

  def this(fileLocation : String = System.getProperty("expenses.file.location")){

    this({{
      if (fileLocation.isEmpty /*|| Option.apply(Paths.get(fileLocation))).Left*/) {

      }
      fromFile(fileLocation)
    }})
  }


  override def getExpenses(): Seq[Expense] = {
    //TODO Change to use month as input

    val expenseList = new ListBuffer[Expense]

    bufferedSource.getLines().drop(1).foreach( line => {
        val Array(expenseCategory, timestamp, owner, fullAmount, divisionType) = line.split(",").map(field => field.trim)

        expenseList+=new Expense(expenseCategory, new SimpleDateFormat("dd-MM-yyyy").parse(timestamp).getTime, owner, fullAmount.toInt, divisionType)
    })

    expenseList.toList
  }

}
