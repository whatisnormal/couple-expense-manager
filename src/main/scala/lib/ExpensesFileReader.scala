package lib

import java.text.SimpleDateFormat

import api.models.Expense

import scala.io.Source
import scala.util.Try

/**
  * FIXME
  *
  * @author José Fernandes (jose.fernandes@feedzai.com)
  * @since @@@feedzai.next.release@@@
  */
object ExpensesFileReader {

  /**
    * Reads a file from a specific path and applies the given transforming function to the input source.
    *
    * @param path The filesystem path from which the expense  file will be read.
    * @param f The transforming function that maps file lines to [[Expense]].
    * @return The [[Seq[Expense]]]
    */
  def apply(path : String , f :  String => Expense): Seq[Expense] = {
    require(path!= null && !path.isEmpty, "File path must not be null or empty.")
    require(f!=null, "Mapping function cannot be null.")

    Source.fromFile(path).getLines().drop(0).map(f).toList
  }

  def apply(path : String) : Seq[Expense] = {
    val function = (line : String) => {
      val Array(expenseCategory, timestamp, owner, fullAmount, divisionType) = line.split(",").map(field => field.trim)
      new Expense(expenseCategory, new SimpleDateFormat("dd-MM-yyyy").parse(timestamp).getTime, owner, fullAmount.toInt, divisionType)
    }
    apply(path, function)
  }

}
