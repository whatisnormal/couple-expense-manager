package api.adapters

import api.models.Expense

/**
  * Reads expenses parsed information from an external source.
  *
  */
trait InputReader {

  /**
    * Loads a list of expenses.
    *
    * @return The [[Expense]] list.
    */
  def getExpenses() : Seq[Expense];

}
