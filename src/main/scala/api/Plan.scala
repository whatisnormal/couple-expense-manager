package api

trait Plan {

  def doWork() : Map[String, Double]
}
