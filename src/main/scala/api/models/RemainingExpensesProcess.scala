package api.models

import lib.{Calculator, ExpensesFileReader, YamlConfigReader}

/**
  * FIXME
  *
  * @author José Fernandes (jose.fernandes@feedzai.com)
  * @since @@@feedzai.next.release@@@
  */
object RemainingExpensesProcess {

  def apply(configFilePath : String, expensesFilePath : String) : Unit = {

    val metadata = YamlConfigReader(configFilePath)
    val expenses = ExpensesFileReader(expensesFilePath)

    val map = Calculator(metadata, expenses)
    //TODO Write to pay amount.
  }
}
