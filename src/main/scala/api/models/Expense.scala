package api.models

/** The expense event related to a purchase transaction.
  *
  * @constructor Create a new expense record with the following set of fields.
  * @param expenseCategory The expense category as in food, triple-play, gas, eletricity.
  * @param timestamp       The date in which the expense was processed.
  * @param owner           The expense purchaser. Ie: Who bought the resources.
  * @param amount          The expense amount in € in whole format -> ie: Purchase of 100.10 = 10010
  * @param divisonType     Differentiator of how the expense is to be splitted. Can be Half or custom split
  */
class Expense(val expenseCategory : String,
              val  timestamp : Long,
              val     owner : String,
              val   fullAmount : Int,
              val   divisonType : String)  {

  require(!expenseCategory.isEmpty)
  require(timestamp>0L)
  require(!owner.isEmpty)
  require(fullAmount>0)
  require(!divisonType.isEmpty)

  override def toString: String = {
    "expenseCategory: "+ expenseCategory+"| timestamp: "+timestamp+"|owner: "+owner+"|fullAmount: "+fullAmount+"|divisonType: "+divisonType
  }
}
