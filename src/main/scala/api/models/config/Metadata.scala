package api.models.config

import java.util

import scala.beans.BeanProperty


class Metadata {
  @BeanProperty var members  = new util.ArrayList[String]()
  @BeanProperty var mainMember : String = null
  @BeanProperty var splitPercentage : Float = 0.0f

  override def toString: String =
    s"members: $members, main: $mainMember, splitPercentage: $splitPercentage"

}
