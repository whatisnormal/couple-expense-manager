
import api.models.RemainingExpensesProcess
import api.models.config.Metadata
import lib.{Calculator, ExpensesFileReader, YamlConfigReader}


object ProgramaticCommandLine extends App {

  //TODO validate args input
  require(args.length == 2)


  val expensesFilePath = args(1)
  val configFilePath = args(2)

  RemainingExpensesProcess(configFilePath , expensesFilePath)
}
