
import api.adapters.InputReader
import api.models.Expense
import api.models.config.Metadata
import lib.{CsvReader, LocalPlan}
import pureconfig.loadConfig

import scala.collection.immutable

object Entry {

  def main(args: Array[String]): Unit = {
    loadMetadata (process)
  }

  def loadMetadata(configProcessor : (Metadata) => Unit) = {
    val configEither = loadConfig[Metadata]
    configEither match {
      case Left(ex) => ex.toList.foreach(println)

      case Right(config) =>
        configProcessor(config)
    }
  }

  def process(configMetadata : Metadata): Map[String, Double] = {
    println("Loading metadata")

    val inputReader : InputReader = new CsvReader
    val expenses = inputReader.getExpenses()


    println(s"Just read: ${inputReader.getExpenses()(0).fullAmount}")

    val plan = new LocalPlan(configMetadata, expenses)
    plan.doWork()

  }


}
