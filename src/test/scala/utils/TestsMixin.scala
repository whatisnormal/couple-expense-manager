package utils

import java.io.File

import api.models.Expense

/**
  * FIXME
  *
  * @author José Fernandes (jose.fernandes@feedzai.com)
  * @since @@@feedzai.next.release@@@
  */
trait TestsMixin {

  def generateRandomExpensesData() : Seq[Expense] = {

    val expenseTypes : Seq[String] = Array("supermarket", "triple-play", "electricity", "water")
    val owners : Seq[String] = Array("jose", "joana")
    val divisionType : Seq[String] = Array("half", "split")
    val random = scala.util.Random
    val stream = (1 to 50).toStream

    stream.map(i => {
      new Expense(expenseTypes(random.nextInt(expenseTypes.length)),
        System.currentTimeMillis(),
        owners(random.nextInt(owners.length)),
        random.nextInt(100),
        divisionType(random.nextInt(divisionType.length)))
    }).toSeq
  }

  /**
    * Generates static dummy expenses.
    * With a split percentage of 0.35, calculations should be the following:
    *
    *  remaining for split member: ((1050 + 500 + 2990) * 0.35)  + (3740*0.5)
    *  remaining to main member: 300 * 0.65
    *
    *
    * @return
    */
  def getTestExpensesData() : Seq[Expense] = {
   List(
     new Expense("supermarket", System.currentTimeMillis(), "jose", 1050, "split"),
     new Expense("supermarket", System.currentTimeMillis(), "jose", 500, "split"),
    new Expense("triple-play", System.currentTimeMillis(), "jose", 2990, "split"),
     new Expense("ikea", System.currentTimeMillis(), "joana", 300, "split"),
    new Expense("sushi", System.currentTimeMillis(), "jose", 3740, "half")
    )
  }

  /**
    * Builds a dummy function to be used in [[api.ExpensesFileReader]] tests.
    * @return The dummy function.
    */
  def fileReaderDummyFunction : String => Expense = {
    line: String => new Expense("supermarket", System.currentTimeMillis(), "johndoe", 100000, "split")
  }

  def withTempFile(prefix : String, suffix : String, fun : (File) => Unit) : Unit = {
    val tempFile = File.createTempFile(prefix, suffix)
    try{
      fun(tempFile)
    }finally tempFile.deleteOnExit()
  }
}
