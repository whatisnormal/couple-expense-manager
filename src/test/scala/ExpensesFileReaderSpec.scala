import java.io.{File, FileNotFoundException, PrintWriter}
import java.time._

import lib.ExpensesFileReader
import org.scalatest._
import utils.TestsMixin

import scala.util.{Failure, Success}
/**
  * FIXME
  *
  * @author José Fernandes (jose.fernandes@feedzai.com)
  * @since @@@feedzai.next.release@@@
  */
class ExpensesFileReaderSpec extends FlatSpec with TestsMixin {

  it must "produce IllegalArgumentException when passing invalid parameters to file reader." in {

    //Null path.
    assertThrows[IllegalArgumentException] {
      ExpensesFileReader(null, fileReaderDummyFunction)
    }
    //Empty path.
    assertThrows[IllegalArgumentException] {
      ExpensesFileReader("", fileReaderDummyFunction)
    }
    //Null function
    assertThrows[IllegalArgumentException] {
      ExpensesFileReader("aaaa", null)
    }
    println("Null / empty validations passed.")
  }

  it must "produce FileNotFoundException when passing invalid file path to reader" in {
    assertThrows[FileNotFoundException] {
      ExpensesFileReader("blablablaIamInvalidpath", fileReaderDummyFunction)
    }
    println("Invalid file path passed.")
  }

  it must "succeed when passing a valid file path" in {
    //create temporary file
    val tempFile = File.createTempFile("test",".csv")
    try {
      val expenses = ExpensesFileReader(tempFile.getAbsolutePath, fileReaderDummyFunction)

    }finally tempFile.deleteOnExit()
  }

  it must "return valid expenses from valid file." in {
    val tempFile = File.createTempFile("test", ".csv")
    val writer = new PrintWriter(tempFile)
    val fileContent ="""supermarket,01-10-2018,jose,10000,split
    triple-play,02-10-2018,jose,2900,split
    restaurant,3-10-2018,joana,3000,half""".stripMargin
    writer.write(fileContent)
    writer.flush()

    val expenses = ExpensesFileReader(tempFile.getAbsolutePath)
    // test existence
    assert(expenses.nonEmpty)

    // test count
    assert(expenses.size == 3)

    //test field existence
    expenses.foreach(expense => {
      assert(expense.expenseCategory.nonEmpty)
      assert(Instant.ofEpochMilli(expense.timestamp)!=null)
      assert(expense.owner.nonEmpty)
      assert(expense.fullAmount > 0)
      assert(expense.divisonType.nonEmpty)

    })
    tempFile.deleteOnExit()
  }

}
