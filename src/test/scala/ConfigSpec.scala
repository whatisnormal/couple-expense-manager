import api.models.config.Metadata
import org.scalatest._
import pureconfig.error.ConfigReaderFailures
import pureconfig.loadConfig

import scala.util
import scala.util.Either

class ConfigSpec extends FlatSpec with Matchers {

  "A configurator" should "load values from 'application.conf'" in {
    val config = loadConfig[Metadata]

    config match {
      case Left(ex) => ex.toList.foreach(println)

      case Right(config) =>{
        println(s"${config}")
      }

    }
  }
}
