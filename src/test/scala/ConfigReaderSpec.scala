import java.io.{File, PrintWriter}

import lib.YamlConfigReader
import org.scalatest._
import utils.TestsMixin

class ConfigReaderSpec extends FlatSpec with TestsMixin{

  "Config reader" must "read with success" in {
    val test ="""
      members: [jose, joana]
      mainMember: jose
      splitPercentage: 0.35
      """.stripMargin

      withTempFile("config", "yml", (file : File) => {
        val writer = new PrintWriter(file)
        writer.write(test)
        writer.flush()

        val metadata = YamlConfigReader(file.getAbsolutePath)

        assert(!metadata.members.isEmpty)
        assert(metadata.splitPercentage>0.0f)
        assert(metadata.mainMember.nonEmpty)
      })

  }


}
