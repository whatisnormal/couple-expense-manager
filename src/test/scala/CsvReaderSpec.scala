import java.nio.charset.Charset
import java.nio.file.{Files, OpenOption, Paths}

import lib.CsvReader

import collection.mutable.{ListBuffer, Stack}
import org.scalatest._


class CsvReaderSpec extends FlatSpec with Matchers {

  "A Csv reader " must "read values from a file source" in {
    val file = Files.createTempFile("dummy", "test")
    val filePath = file.toFile.getAbsolutePath
    val list = new ListBuffer[CharSequence]
    list+="a"
    //Files.write(file, list.toList.toIterable, OpenOption. )

    Files.deleteIfExists(file)
  }

  it should "throw NullPointerException if file location is empty or not found" in {
    a [NullPointerException] should be thrownBy {
      new CsvReader("")
    }

    a [NullPointerException] should be thrownBy {
      new CsvReader("/tmp/missinfile.csv")
    }
  }
}