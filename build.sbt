name := "couple-expenses-manager"

version := "0.1"

scalaVersion := "2.12.6"

libraryDependencies += "org.yaml" % "snakeyaml" % "1.8"
libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.5"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % "test"

